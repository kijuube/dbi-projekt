﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models;

namespace SupermarketDeluxe {
    public class Startup {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<SupermarketContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("SupermarketDatabase")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/dist"; });


            // managers
            services.AddScoped<ArticlesManager>();
            services.AddScoped<AddressesManager>();
            services.AddScoped<DepartmentsManager>();
            services.AddScoped<EmployeesManager>();
            services.AddScoped<OrderArticlesManager>();
            services.AddScoped<OrdersManager>();
            services.AddScoped<PriceHistoriesManager>();
            services.AddScoped<SuppliersManager>();

            // data services
            services.AddScoped<ArticlesDataService>();
            services.AddScoped<AddressesDataService>();
            services.AddScoped<DepartmentsDataService>();
            services.AddScoped<EmployeesDataService>();
            services.AddScoped<OrderArticlesDataService>();
            services.AddScoped<OrdersDataService>();
            services.AddScoped<PriceHistoriesDataService>();
            services.AddScoped<SuppliersDataService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
