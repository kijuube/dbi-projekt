﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.DTOs
{
    public class SupplierDto
    {
        public string Name { get; set; }
        public int AddressId { get; set; }

    }
}
