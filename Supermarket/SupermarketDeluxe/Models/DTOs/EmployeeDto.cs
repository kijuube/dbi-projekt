﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.DTOs {
    public class EmployeeDto {
        public string Name { get; set; }
        public int DepartmentId { get; set; }
        public int AddressId { get; set; }
    }
}
