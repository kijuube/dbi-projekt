﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.DTOs
{
    public class ArticleDto
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int IdGivenFromSupplier { get; set; }
        public int SupplierId { get; set; }

    }
}
