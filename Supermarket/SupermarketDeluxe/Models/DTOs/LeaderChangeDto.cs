﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.DTOs {
    public class LeaderChangeDto {
		public int EmployeeId { get; set; }
		public int DepartmentId { get; set; }
    }
}
