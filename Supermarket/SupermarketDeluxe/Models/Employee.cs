﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Operations;

namespace Supermarket.Models {
    public class Employee {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Department Department { get; set; }
        public virtual Address Address { get; set; }
    }
}
