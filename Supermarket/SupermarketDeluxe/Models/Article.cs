﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models {
    public class Article {
        public int ArticleId { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual List<PriceHistory> PriceHistories { get; set; }
        public int IdGivenFromSupplier { get; set; }

        public bool Deleted { get; set; } = false;
    }
}
