﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Supermarket.Models {
    public class Department {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
