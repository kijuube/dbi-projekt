﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Supermarket.Models {
    public class SupermarketContext : DbContext {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderArticle> OrderArticles { get; set; }
        public DbSet<PriceHistory> PriceHistories { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<DepartmentLeader> DepartmentLeaders { get; set; }
        public SupermarketContext(DbContextOptions options) : base(options)
        {
        }
    }
}
