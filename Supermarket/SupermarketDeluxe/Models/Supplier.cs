﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models {
    public class Supplier {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Address Address { get; set; }

        public bool Deleted { get; set; } = false;
    }
}
