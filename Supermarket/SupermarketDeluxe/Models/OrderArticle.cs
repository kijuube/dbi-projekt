﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models {
    public class OrderArticle {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public virtual Article Article { get; set; }
        public virtual Supplier Supplier { get; set; }

    }
}
