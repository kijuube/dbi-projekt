﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.Resources {
    public class DepartmentResource {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LeaderName { get; set; }
        public int LeaderId { get; set; }
    }
}
