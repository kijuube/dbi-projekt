﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.Resources
{
    public class SupplierResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public AddressResource Address { get; set; }

        public bool Deleted { get; set; }
    }
}
