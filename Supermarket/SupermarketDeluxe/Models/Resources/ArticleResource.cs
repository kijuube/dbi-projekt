﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.Resources
{
    public class ArticleResource
    {
        public int ArticleId { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int IdGivenFromSupplier { get; set; }
        public string Supplier { get; set; }

    }
}
