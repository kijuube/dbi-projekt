﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.Resources
{
    public class OrderResource
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }

    }
}
