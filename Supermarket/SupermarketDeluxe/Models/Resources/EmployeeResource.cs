﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models.Resources
{
    public class EmployeeResource
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public DepartmentResource Department { get; set; }
        public AddressResource Address { get; set; }

    }
}
