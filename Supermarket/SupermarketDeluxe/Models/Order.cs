﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Models {
    public class Order {
        public int Id { get; set; }
        public DateTime? OrderDate { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual List<OrderArticle> OrderArticles { get; set; }
    }
}
