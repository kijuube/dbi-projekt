﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Supermarket.Models;

namespace Supermarket.Managers
{
    public class PriceHistoriesManager : BaseManager {
        private ArticlesManager articlesManager;

        public PriceHistoriesManager(SupermarketContext context, ArticlesManager articlesManager) : base(context) {
            this.articlesManager = articlesManager;
        }

        internal IEnumerable<PriceHistory> GetPriceHistories()
        {
            return Context.PriceHistories.ToList();
        }

        internal PriceHistory GetPriceHistoryForId(int id)
        {
            return Context.PriceHistories
                .FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<PriceHistory> GetPriceHistoryForArticleId(int id) {
            return articlesManager.GetArticleForId(id).PriceHistories.ToList();
        }
    }
}
