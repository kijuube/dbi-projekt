﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Supermarket.Models;

namespace Supermarket.Managers {
    public class OrdersManager : BaseManager {
        public OrdersManager(SupermarketContext context) : base(context) {
        }

        internal IEnumerable<Order> GetOrders() {
            return Context.Orders.ToList();
        }

        internal Order GetOrderForId(int id) {
            return Context.Orders.Include(x => x.OrderArticles)
                .FirstOrDefault(x => x.Id == id);
        }
    }
}
