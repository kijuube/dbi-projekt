﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Supermarket.Models;

namespace Supermarket.Managers {
    public class EmployeesManager : BaseManager {
        public EmployeesManager(SupermarketContext context) : base(context) {
        }

        internal IEnumerable<Employee> GetEmployees() {
            return Context.Employees.Include(x => x.Address).Include(x => x.Department).ToList();
        }

        internal Employee GetEmployeeForId(int id) {
            return Context.Employees
                .Include(x => x.Address).Include(x => x.Department)
                .FirstOrDefault(x => x.Id == id);
        }

        public Employee CreateEmployee(Employee employee) {
            var e = Context.Employees.Add(employee);
            Context.SaveChanges();
            return e.Entity;
        }
    }
}
