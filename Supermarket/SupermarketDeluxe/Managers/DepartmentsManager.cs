﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Supermarket.Models;
using Supermarket.Models.Resources;

namespace Supermarket.Managers
{
    public class DepartmentsManager : BaseManager
    {
        public DepartmentsManager(SupermarketContext context) : base(context)
        {
        }

        internal IEnumerable<Department> GetDepartments()
        {
            return Context.Departments.ToList();
        }

        internal Department GetDepartmentForId(int id)
        {
            return Context.Departments.FirstOrDefault(x => x.Id == id);
        }

        public DepartmentLeader GetDepartmentLeader(Department department) {
            var leader = Context.DepartmentLeaders.Include(x => x.Department).Include(x => x.Employee)
                .FirstOrDefault(x => x.Department.Id == department.Id);
            return leader;
        }

        public DepartmentLeader ChangeLeader(DepartmentLeader leader) {
            if (Context.Entry(leader).State == EntityState.Detached) {
                leader = Context.DepartmentLeaders.Add(leader).Entity;
            }

            Context.SaveChanges();
            return leader;
        }
    }
}
