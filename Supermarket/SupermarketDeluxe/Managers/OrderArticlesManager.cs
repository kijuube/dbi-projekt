﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Supermarket.Models;

namespace Supermarket.Managers
{
    public class OrderArticlesManager : BaseManager {

        private OrdersManager ordersManager;

        public OrderArticlesManager(SupermarketContext context, OrdersManager ordersManager) : base(context) {
            this.ordersManager = ordersManager;
        }

        internal IEnumerable<OrderArticle> GetOrderArticles()
        {
            return Context.OrderArticles.ToList();
        }

        internal OrderArticle GetOrderArticleForId(int id)
        {
            return Context.OrderArticles
                .FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<OrderArticle> GetOrderArticlesForOrder(int orderId) {
            var order = ordersManager.GetOrderForId(orderId);
            return order.OrderArticles;
        }
    }
}
