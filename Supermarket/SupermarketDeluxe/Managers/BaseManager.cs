﻿using Supermarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Managers
{
    public abstract class BaseManager
    {
        protected SupermarketContext Context { get; private set; }

        public BaseManager(SupermarketContext context)
        {
            this.Context = context;
        }

    }
}
