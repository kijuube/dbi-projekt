﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Supermarket.Models;
using Supermarket.Models.DTOs;
using Supermarket.Models.Resources;

namespace Supermarket.Managers {
    public class ArticlesManager : BaseManager {
        public ArticlesManager(SupermarketContext context) : base(context) {
        }

        internal IEnumerable<Article> GetArticles() {
            return Context.Articles.Include(x => x.Supplier).ToList();
        }

        internal Article GetArticleForId(int id) {
            return Context.Articles.Include(x => x.PriceHistories).Include(x => x.Supplier).FirstOrDefault(x => x.ArticleId == id);
        }

        internal Article PostArticle(Article article) {
            var e = Context.Articles.Add(article);
            Context.SaveChanges();
            return e.Entity;
        }

        internal void PutArticle(Article value) {
            Context.Articles.Update(value);
            Context.SaveChanges();
        }

        internal void DeleteArticle(int id) {
            Article article = (Article) Context.Articles.Where(x => x.ArticleId == id);
            article.Deleted = true;
            Context.SaveChanges();
        }
    }
}
