﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Supermarket.Models;
using Supermarket.Models.DTOs;

namespace Supermarket.Managers {
    public class SuppliersManager : BaseManager {
        public SuppliersManager(SupermarketContext context) : base(context) {
        }

        internal IEnumerable<Supplier> GetSuppliers() {
            return Context.Suppliers.Include(x => x.Address).ToList();
        }

        internal Supplier GetSupplierForId(int id) {
            return Context.Suppliers
                .Include(x => x.Address)
                .FirstOrDefault(x => x.Id == id);
        }

        internal Supplier PostSupplier(Supplier supplier) {
            var e = Context.Suppliers.Add(supplier);
            Context.SaveChanges();
            return e.Entity;
        }

        internal void DeleteSupplier(int id) {
            var supplier = Context.Suppliers.FirstOrDefault(x => x.Id == id);
            if (supplier != null) supplier.Deleted = true;
            Context.SaveChanges();
        }
    }
}
