﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Supermarket.Models;

namespace Supermarket.Managers {
    public class AddressesManager : BaseManager {
        public AddressesManager(SupermarketContext context) : base(context) {
        }

        internal Address GetAddressForId(int id) {
            return Context.Addresses
                .FirstOrDefault(x => x.Id == id);
        }

        internal List<Address> GetAddresses() {
            return Context.Addresses.ToList();
        }

        public Address CreateAddress(Address address) {
            var entry = Context.Addresses.Add(address);
            Context.SaveChanges();
            return entry.Entity;
        }
    }
}
