﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models.Resources;

namespace Supermarket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderArticlesController : BaseController<OrderArticlesDataService, OrderArticlesManager>
    {
        public OrderArticlesController(OrderArticlesDataService dataService) : base(dataService)
        {
        }

        // GET: api/OrderArticles
        [HttpGet]
        public IEnumerable<OrderArticleResource> Get()
        {
            return DataService.GetOrderArticles();
        }

        // GET: api/OrderArticles/5
        [HttpGet("{orderId}")]
        public IEnumerable<OrderArticleResource> Get(int orderId)
        {
            return DataService.GetOrderArticlesForOrder(orderId);
        }
    }
}
