﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models.DTOs;
using Supermarket.Models.Resources;

namespace Supermarket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuppliersController : BaseController<SuppliersDataService, SuppliersManager>
    {
        public SuppliersController(SuppliersDataService dataService) : base(dataService)
        {
        }

        // GET: api/Suppliers
        [HttpGet]
        public IEnumerable<SupplierResource> Get()
        {
            return DataService.GetSuppliers();
        }

        // GET: api/Suppliers/5
        [HttpGet("{id}")]
        public SupplierResource Get(int id)
        {
            return DataService.GetSupplierForId(id);
        }

        // POST: api/Suppliers
        [HttpPost]
        public SupplierResource Post([FromBody] SupplierDto supplier)
        {
            return DataService.PostSupplier(supplier);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            DataService.DeleteSupplier(id);
        }
    }
}
