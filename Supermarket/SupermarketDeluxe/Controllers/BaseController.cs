﻿using Supermarket.DataServices;
using Supermarket.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.Controllers
{
    public class BaseController<TDataService, TManager> where TManager : BaseManager where TDataService : BaseDataService<TManager>
    {

        public TDataService DataService { get; private set; }

        public BaseController(TDataService dataService)
        {
            this.DataService = dataService;
        }


    }
}
