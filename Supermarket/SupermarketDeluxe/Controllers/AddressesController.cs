﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models.DTOs;
using Supermarket.Models.Resources;

namespace Supermarket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : BaseController<AddressesDataService, AddressesManager>
    {
        public AddressesController(AddressesDataService dataService) : base(dataService)
        {
        }

        // GET: api/Adresses
        [HttpGet]
        public IEnumerable<AddressResource> GetAddresses()
        {
            return DataService.GetAddresses();
        }

        [HttpPost]
        public AddressResource CreateAddress([FromBody] AddressDto dto) {
            return DataService.CreateAddress(dto);
        }

        // GET: api/Adresses/5
        [HttpGet("{id}")]
        public AddressResource GetAddressForId(int id)
        {
            return DataService.GetAddressForId(id);
        }
    }
}
