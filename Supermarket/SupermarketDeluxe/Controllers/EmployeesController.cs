﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.DTOs;
using Supermarket.Models.Resources;

namespace Supermarket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : BaseController<EmployeesDataService, EmployeesManager>
    {
        public EmployeesController(EmployeesDataService dataService) : base(dataService)
        {
        }


        // GET: api/Employees
        [HttpGet]
        public IEnumerable<EmployeeResource> Get()
        {
            return DataService.GetEmployees();
        }

        // GET: api/Employees/5
        [HttpGet("{id}")]
        public EmployeeResource Get(int id)
        {
            return DataService.GetEmployeeForId(id);
        }

        [HttpPost]
        public EmployeeResource Create([FromBody] EmployeeDto employee) {
            return DataService.CreateEmployee(employee);
        }
    }
}
