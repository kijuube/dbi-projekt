﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.Resources;

namespace Supermarket.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : BaseController<OrdersDataService, OrdersManager> {
        public OrdersController(OrdersDataService dataService) : base(dataService) {
        }

        // GET: api/Orders
        [HttpGet]
        public IEnumerable<OrderResource> Get() {
            return DataService.GetOrders();
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public OrderResource Get(int id) {
            return DataService.GetOrderForId(id);
        }
    }
}
