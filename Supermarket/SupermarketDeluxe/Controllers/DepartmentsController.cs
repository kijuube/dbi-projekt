﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models.DTOs;
using Supermarket.Models.Resources;

namespace Supermarket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : BaseController<DepartmentsDataService, DepartmentsManager>
    {
        public DepartmentsController(DepartmentsDataService dataService) : base(dataService)
        {
        }

        // GET: api/Departments
        [HttpGet]
        public IEnumerable<DepartmentResource> Get()
        {
            return DataService.GetDepartments();
        }

        [HttpPut]
        public DepartmentResource ChangeLeader([FromBody] LeaderChangeDto dto) {
            return DataService.ChangeLeader(dto);
        }

        // GET: api/Departments/5
        [HttpGet("{id}")]
        public DepartmentResource Get(int id)
        {
            return DataService.GetDepartmentForId(id);
        }
    }
}
