﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models.Resources;

namespace Supermarket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriceHistoriesController : BaseController<PriceHistoriesDataService, PriceHistoriesManager>
    {
        public PriceHistoriesController(PriceHistoriesDataService dataService) : base(dataService)
        {
        }

        // GET: api/PriceHistories
        [HttpGet]
        public IEnumerable<PriceHistoryResource> Get()
        {
            return DataService.GetPriceHistories();
        }

        // GET: api/PriceHistories/articleId
        [HttpGet("{id}")]
        public IEnumerable<PriceHistoryResource> Get(int id)
        {
            return DataService.GetPriceHistoryForArticleId(id);
        }
    }
}
