﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supermarket.DataServices;
using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.DTOs;
using Supermarket.Models.Resources;

namespace Supermarket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : BaseController<ArticlesDataService, ArticlesManager>
    {
        public ArticlesController(ArticlesDataService dataService) : base(dataService)
        {
        }



        // GET: api/Articles
        [HttpGet]
        public IEnumerable<ArticleResource> GetArticle() 
        {
            return DataService.GetArticles();
        }

        // GET: api/Articles/5
        [HttpGet("{id}")]
        public ArticleResource Get(int id)
        {
            return DataService.GetArticleForId(id);
        }

        // POST: api/Articles
        [HttpPost]
        public ArticleResource Post([FromBody] ArticleDto article)
        {
            return DataService.PostArticle(article); 
        }

        // PUT: api/Articles/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ArticleDto value)
        {
            DataService.PutArticle(value, id);   
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            DataService.DeleteArticle(id);
        }
    }
}
