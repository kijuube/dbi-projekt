﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Supermarket.Models;

namespace Supermarket {
    public class DbSeeder
    {
        private SupermarketContext db;

        public DbSeeder(SupermarketContext db)
        {
            this.db = db;
            ResetDatabase();
            AddAddresses();
            AddSuppliers();
            AddArticles();
            AddPriceHistories();
            AddEmployees();
            AddDepartments();
            AddOrders();
            AddOrderArticles();
            db.SaveChanges();
        }

        private void AddOrderArticles()
        {
            using (StreamReader r = new StreamReader("addressJSON.json")) {
                string json = r.ReadToEnd();
                List<Address> items = JsonConvert.DeserializeObject<List<Address>>(json);
                Console.WriteLine();
            }
        }

        private void AddOrders()
        {
            
        }

        private void AddDepartments()
        {
            
        }

        private void AddEmployees()
        {
            
        }

        private void AddPriceHistories()
        {
            
        }

        private void AddArticles()
        {
            
        }

        private void AddSuppliers()
        {
            
        }

        private void AddAddresses()
        {
            
        }

        private void ResetDatabase()
        {
        }
    }
}
