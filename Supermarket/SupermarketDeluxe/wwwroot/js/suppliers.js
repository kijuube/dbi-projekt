﻿getSuppliers().then(data => {
    data.forEach(x => x.address = addressToString(x.address));
    setData(data);
}).catch(e => {
    console.log(e);
    });


$("#submit").click(() => {
    console.log("creating supplier");
    const address = {
        zipCode: $("#plz").val(),
        city: $("#city").val(),
        street: $("#street").val(),
        streetNumber: $("#houseNumber").val()
    };

    console.log("posting address: ", address);

    postAddress(address).then(x => {
        const supplier = {
            name: $("#name").val(),
            addressId: x.id
        }

        console.log("got address", address);
        console.log("built supplier", supplier);

        postSupplier(supplier).then(x => {
            console.log("POSTED", x);
            location.reload();
        }).catch(x => {
            console.error("ERROR", x);
        });
    }).catch(x => {
        console.log(x);
    });
});