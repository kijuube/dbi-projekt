﻿const priceHtml = "<a id=\"$$VIEWID$$\" href=\"#\">Ansehen</button>";


const clickView = (article) => {
    setModalTitle(`${article.name}`);
    createSelector().then(x => {
        openModal();
    });
}

const createSelector = () => {
    const body = editModalBody();
    const select = $("<select>").attr("id", "selLeader");
    $("#modal-ok").click(() => {
        closeModal();
    });
    return Promise.resolve(0);
};

getArticles().then(data => {
    console.log(data);
    data.forEach(transform);
    setData(data);
    data.forEach(x => {
        $(`#view_${x.articleId}`).click(() => clickView(x));
    });
}).catch(e => {
    console.log(e);
});

const transform = (article) => {
    article.price = article.price + " €"; //;       " + priceHtml.replace("$$VIEWID$$", `view_${article.articleId}`);
}


getSuppliers().then(data => {
    const select = $("#suppliers");
    console.log("got suppliers");
    data.forEach(s => {
        $("<option>").html(s.name).attr("data-id", `${s.id}`).appendTo(select);
    });
});

$("#submit").click(() => {
    const article = {
        supplierId: $("#suppliers option:selected").attr("data-id") + "",
        name: $("#name").val(),
        price: $("#price").val()
    }

    postArticle(article).then(x => {
        console.log("POSTED", x);
    });
});