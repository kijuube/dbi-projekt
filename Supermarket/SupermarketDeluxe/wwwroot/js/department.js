﻿const leaderHtml = "<button id=\"$$SETID$$\" href=\"#\">Ändern</button>";

let currDept = 0;

const clickChange = (id, name) => {
    currDept = id;
    setModalTitle(`Abteilungsleiter setzen für ${name}`);
    createSelector().then(x => {
        openModal();
    });
}

const transform = (dept) => {
    const id = `set_${dept.id}`;
    dept.leaderName =
        dept.leaderName + "       " + leaderHtml.replace("$$NAME$$", dept.leaderName).replace("$$SETID$$", id);
}

const setLeader = (empId, deptId) => {
    put("departments", { EmployeeId: empId, DepartmentId: deptId }).then(x => {
        console.log(x);
    });
};

const createSelector = () => {
    const body = editModalBody();
    const select = $("<select>").attr("id", "selLeader");
    $("#modal-ok").click(() => {
        closeModal();
        const empId = $("#selLeader option:selected").attr("data-id") + "";
        setLeader(empId, currDept);
    });
    body.append(select);
    return getEmployees().then(data => {
        data.forEach(emp => {
            const opt = $("<option>").text(emp.name).attr("data-id", emp.id);
            select.append(opt);
        });

        return data;
    });
};

getDepartments().then(data => {
    data.forEach(transform);
    setData(data);
    data.forEach(x => {
        $(`#set_${x.id}`).click(() => clickChange(x.id, x.name));
    });
}).catch(e => {
    console.log(e);
});