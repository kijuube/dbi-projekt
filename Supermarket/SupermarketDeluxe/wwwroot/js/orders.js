﻿const orderTemplate = "<button href=\"#\" id=\"$$VIEWID$$\">Ansehen</button>";

const clickView = (order) => {
    setModalTitle(`Bestellung #${order.id}`);
    const body = editModalBody();
    getOrderArticles(order.id).then(articles => {
        console.log("got articles", articles);
        const list = $("<ul>");
        articles.forEach(article => {
            const element = $("<li>").text(`${article.quantity}x , für ${round(article.unitPrice)}€ pro Stück, Gesamtpreis: ${round(article.quantity*article.unitPrice)}€`);
            list.append(element);
        });

        body.append(list);
        $("#modal-ok").click(() => {
            closeModal();
        });
        openModal();
    });
};

const transform = (order) => {
    order.view = orderTemplate.replace("$$VIEWID$$", `view_${order.id}`);
}

getOrders().then(data => {
    console.log("got orders", data);
    data.forEach(transform);
    setData(data);
    data.forEach(x => {
        $(`#view_${x.id}`).click(() => {
            clickView(x);
        });
    });
}).catch(e => {
    console.log(e);
})