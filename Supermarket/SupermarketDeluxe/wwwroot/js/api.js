﻿const baseUrl = "http://localhost:5000/api";

const $_GET = (q, s) => {
    s = s ? s : window.location.search;
    var re = new RegExp(`&${q}(?:=([^&]*))?(?=&|$)`, "i");
    return (s = s.replace(/^\?/, "&").match(re))
        ? (typeof s[1] == "undefined" ? "" : decodeURIComponent(s[1]))
        : undefined;
}

const round = (nr) => Math.round(nr * 100) / 100;

const getEmployees = () => {
    return $.getJSON(`${baseUrl}/employees`);
}

const getDepartments = () => {
    return $.getJSON(`${baseUrl}/departments`);
}

const getOrderArticles = (orderId) => {
    return $.getJSON(`${baseUrl}/orderarticles/${orderId}`);
}

const getArticles = () => {
    return $.getJSON(`${baseUrl}/articles`);
}

const getSuppliers = () => {
    return $.getJSON(`${baseUrl}/suppliers`);
}

const getOrders = () => {
    return $.getJSON(`${baseUrl}/orders`);
}

const openModal = () => {
    getModal().modal({ show: true });
}

const getModal = () => {
    return $("#modal");
}

const closeModal = () => {
    getModal().modal("hide");
}

const editModalBody = () => {
    $("#modal-body").empty();
    return $("#modal-body");
}

const setModalTitle = (title) => {
    $("#modal-title").text(title);
}


const postAddress = (address) => {
    return post("addresses", address);
}

const post = (url, data) => {
    return $.ajax({
        url: `${baseUrl}/${url}`,
        type: "post",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data)
    }).catch(e => {
        console.error("ERROR", e);
    }).then(x => {
        location.reload();
        return x;
    });
}

const put = (url, data) => {
    return $.ajax({
        url: `${baseUrl}/${url}`,
        type: "put",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data)
    }).catch(e => {
        console.error("ERROR", e);
    }).then(x => {
        location.reload();
        return x;
    });
}

const postArticle = (article) => {
    return post("articles", article);
}

const postSupplier = (supplier) => {
    return post("suppliers", supplier);
}

const postEmployee = (employee) => {
    return post("employees", employee);
}

const search = "https://www.google.at/maps/search/";

const getGmapsUrl = (address) => {
    return `${search}${address.street}, ${address.city}`;
}

const addressToString = (x) => {
    return `<a target=\"_blank\" rel=\"noopener noreferrer\" href=\"${getGmapsUrl(x)}\">${x.streetNumber} ${x.street
        }, ${x.zipcode} ${x.city}</a>`;
}