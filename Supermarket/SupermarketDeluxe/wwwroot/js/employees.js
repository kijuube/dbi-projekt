﻿getEmployees().then(data => {
    console.log(data);
    data.forEach(x => {
        x.address = addressToString(x.address);
        x.department = x.department.name;
    });
    setData(data);
}).catch(e => {
    console.log(e);
});

getDepartments().then(data => {
    console.log(data);
    console.log("got departments");
    const select = $("#department");
    data.forEach(s => {
        $("<option>").html(s.name).attr("data-id", `${s.id}`).appendTo(select);
    });
});

$("#post").click(() => {
    console.log("creating employee");
    const address = {
        zipCode: $("#plz").val(),
        city: $("#city").val(),
        street: $("#street").val(),
        streetNumber: $("#houseNumber").val()
    };

    console.log("posting address: ", address);

    postAddress(address).then(x => {
        const employee = {
            name: $("#name").val(),
            addressId: x.id,
            departmentId: $("#department option:selected").attr("data-id") + ""
        }

        console.log("got address", address);
        console.log("built supplier", employee);

        postEmployee(employee).then(x => {
            console.log("POSTED", x);
            location.reload();
        }).catch(x => {
            console.error("ERROR", x);
        });
    }).catch(x => {
        console.log(x);
    });
})