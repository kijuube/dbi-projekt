﻿

$(document).ready(() => {
    $("#superDataTable").DataTable();
});

const getTable = () => $("#superDataTable").DataTable();

const setData = (data) => {
    const table = getTable();
    const arr = data.map(obj => {
        const res = [];
        for (const x in obj) {
            if (obj.hasOwnProperty(x)) {
                res.push(obj[x]);
            }
        }
        return res;
    });
    table.clear();
    table.rows.add(arr);
    table.draw();
}