﻿using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.DataServices {
    public class OrderArticlesDataService : BaseDataService<OrderArticlesManager> {
        public OrderArticlesDataService(OrderArticlesManager manager) : base(manager) {
        }

        internal IEnumerable<OrderArticleResource> GetOrderArticles() {
            return Manager.GetOrderArticles().Select(ConvertToResource<OrderArticleResource, OrderArticle>);
        }

        public IEnumerable<OrderArticleResource> GetOrderArticlesForOrder(int orderId) {
            return Manager.GetOrderArticlesForOrder(orderId)
                .Select(ConvertToResource<OrderArticleResource, OrderArticle>);
        }
    }
}
