﻿using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.DataServices {
    public class PriceHistoriesDataService : BaseDataService<PriceHistoriesManager> {
        public PriceHistoriesDataService(PriceHistoriesManager manager) : base(manager) {
        }

        internal IEnumerable<PriceHistoryResource> GetPriceHistories() {
            return Manager.GetPriceHistories().Select(ConvertToResource<PriceHistoryResource, PriceHistory>);
        }

        public IEnumerable<PriceHistoryResource> GetPriceHistoryForArticleId(int id) {
            return Manager.GetPriceHistoryForArticleId(id)
                .Select(ConvertToResource<PriceHistoryResource, PriceHistory>);
        }
    }
}
