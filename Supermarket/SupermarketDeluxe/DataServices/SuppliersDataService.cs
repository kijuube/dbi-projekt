﻿using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.DTOs;
using Supermarket.Models.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.DataServices {
    public class SuppliersDataService : BaseDataService<SuppliersManager> {
        private readonly AddressesManager addressesManager;

        public SuppliersDataService(SuppliersManager manager, AddressesManager addressesManager) : base(manager) {
            this.addressesManager = addressesManager;
        }

        internal IEnumerable<SupplierResource> GetSuppliers() {
            return Manager.GetSuppliers().Select(ConvertToSupplierResource);
        }

        private SupplierResource ConvertToSupplierResource(Supplier supplier) {
            var resource = ConvertToResource<SupplierResource, Supplier>(supplier);
            resource.Address = ConvertToResource<AddressResource, Address>(supplier.Address);
            return resource;
        }

        internal SupplierResource GetSupplierForId(int id) {
            return ConvertToSupplierResource(Manager.GetSupplierForId(id));
        }

        internal SupplierResource PostSupplier(SupplierDto supplier) {
            return ConvertToSupplierResource(Manager.PostSupplier(ConvertToSupplier(supplier)));
        }

        private Supplier ConvertToSupplier(SupplierDto supplierDto) {
            var address = addressesManager.GetAddressForId(supplierDto.AddressId);
            Console.WriteLine(address.Id);
            return new Supplier {
                Name = supplierDto.Name,
                Address = address
            };
        }

        internal void DeleteSupplier(int id) {
            Manager.DeleteSupplier(id);
        }
    }
}
