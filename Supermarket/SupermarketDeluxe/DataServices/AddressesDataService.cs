﻿using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.Resources;
using System.Collections.Generic;
using System.Linq;
using Supermarket.Models.DTOs;

namespace Supermarket.DataServices {
    public class AddressesDataService : BaseDataService<AddressesManager> {

        public AddressesDataService(AddressesManager manager) : base(manager) {
        }

        internal AddressResource GetAddressForId(int id) {
            return ConvertToResource<AddressResource, Address>(Manager.GetAddressForId(id));
        }

        internal IEnumerable<AddressResource> GetAddresses() {
            return Manager.GetAddresses()
                .Select(ConvertToResource<AddressResource, Address>)
                .ToList();
        }

        private Address ConvertToAddress(AddressDto dto) {
            var address = new Address() {
                City = dto.City,
                Street = dto.Street,
                StreetNumber = dto.StreetNumber,
                Zipcode = dto.ZipCode,
            };

            return address;
        }

        public AddressResource CreateAddress(AddressDto dto) {
            return ConvertToResource<AddressResource, Address>(Manager.CreateAddress(ConvertToAddress(dto)));
        }
    }
}
