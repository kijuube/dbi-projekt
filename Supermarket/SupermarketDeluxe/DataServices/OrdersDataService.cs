﻿using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.DataServices
{
    public class OrdersDataService : BaseDataService<OrdersManager>
    {
        public OrdersDataService(OrdersManager manager) : base(manager)
        {
        }

        internal IEnumerable<OrderResource> GetOrders()
        {
            return Manager.GetOrders().Select(x => ConvertToResource<OrderResource, Order>(x));
        }

        internal OrderResource GetOrderForId(int id)
        {
            return ConvertToResource<OrderResource, Order>(Manager.GetOrderForId(id));
        }
    }
}
