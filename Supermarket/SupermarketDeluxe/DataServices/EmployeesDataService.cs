﻿using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Supermarket.Models.DTOs;

namespace Supermarket.DataServices
{
    public class EmployeesDataService : BaseDataService<EmployeesManager> {

        private readonly AddressesManager addressesManager;
        private readonly DepartmentsManager departmentsManager;

        public EmployeesDataService(EmployeesManager manager, AddressesManager addressesManager, DepartmentsManager departmentsManager) : base(manager) {
            this.addressesManager = addressesManager;
            this.departmentsManager = departmentsManager;
        }

        internal IEnumerable<EmployeeResource> GetEmployees()
        {
            return Manager.GetEmployees().Select(ConvertToEmployeeResource);
        }

        private Employee ConvertToEmployee(EmployeeDto dto) {
            var employee = new Employee {
                Name = dto.Name,
                Department = departmentsManager.GetDepartmentForId(dto.DepartmentId),
                Address = addressesManager.GetAddressForId(dto.AddressId)
            };

            return employee;
        }

        private EmployeeResource ConvertToEmployeeResource(Employee employee) {
            var res = ConvertToResource<EmployeeResource, Employee>(employee);
            res.Address = ConvertToResource<AddressResource, Address>(employee.Address);
            res.Department = ConvertToResource<DepartmentResource, Department>(employee.Department);
            return res;
        }

        internal EmployeeResource GetEmployeeForId(int id)
        {
            return ConvertToEmployeeResource(Manager.GetEmployeeForId(id));
        }

        public EmployeeResource CreateEmployee(EmployeeDto employee) {
            return ConvertToEmployeeResource(Manager.CreateEmployee(ConvertToEmployee(employee)));
        }
    }
}
