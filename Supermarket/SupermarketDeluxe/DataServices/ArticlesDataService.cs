﻿using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.DTOs;
using Supermarket.Models.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Supermarket.DataServices {
    public class ArticlesDataService : BaseDataService<ArticlesManager> {
        private SuppliersManager suppliersManager;

        public ArticlesDataService(ArticlesManager manager, SuppliersManager suppliersManager) : base(manager) {
            this.suppliersManager = suppliersManager;
        }

        internal IEnumerable<ArticleResource> GetArticles() {
            return Manager.GetArticles()
                .Select(ConvertToArticleResource)
                .ToList();
        }

        private ArticleResource ConvertToArticleResource(Article article) {
            var resource = ConvertToResource<ArticleResource, Article>(article);
            resource.Supplier = article.Supplier.Name;
            return resource;
        }

        internal ArticleResource GetArticleForId(int id) {
            return ConvertToArticleResource(Manager.GetArticleForId(id));
        }

        private Article ConvertToArticle(ArticleDto articleDto, int id) {
            var article = Manager.GetArticleForId(id);
            if (article == null) article = new Article();
            if(id >= 0) article.ArticleId = id;
            if (article.ArticleId < 0) article.ArticleId = 0;

            article.Name = articleDto.Name;
            article.Price = articleDto.Price;
            article.IdGivenFromSupplier = articleDto.IdGivenFromSupplier;
            article.Supplier = suppliersManager.GetSupplierForId(articleDto.SupplierId);
            return article;
        }

        internal ArticleResource PostArticle(ArticleDto article) {
            return ConvertToArticleResource(Manager.PostArticle(ConvertToArticle(article, -1)));
        }

        internal void PutArticle(ArticleDto value, int id) {
            Article article = ConvertToArticle(value, id);
            Manager.PutArticle(article);
        }

        internal void DeleteArticle(int id) {
            Manager.DeleteArticle(id);
        }
    }
}
