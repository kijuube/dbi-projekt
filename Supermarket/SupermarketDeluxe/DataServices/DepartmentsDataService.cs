﻿using Supermarket.Managers;
using Supermarket.Models;
using Supermarket.Models.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Supermarket.Models.DTOs;

namespace Supermarket.DataServices {
    public class DepartmentsDataService : BaseDataService<DepartmentsManager> {
        private EmployeesManager employeesManager;

        public DepartmentsDataService(DepartmentsManager manager, EmployeesManager employeesManager) : base(manager) {
            this.employeesManager = employeesManager;
        }

        private DepartmentResource ConvertToDepartmentResource(Department department) {
            var resource = ConvertToResource<DepartmentResource, Department>(department);
            var leader = Manager.GetDepartmentLeader(department);
            resource.LeaderName = "keiner";
            resource.LeaderId = 0;
            if (leader != null) {
                resource.LeaderName = leader.Employee.Name;
                resource.LeaderId = leader.Employee.Id;
            }

            return resource;
        }

        internal IEnumerable<DepartmentResource> GetDepartments() {
            return Manager.GetDepartments().Select(ConvertToDepartmentResource);
        }

        internal DepartmentResource GetDepartmentForId(int id) {
            return ConvertToDepartmentResource(Manager.GetDepartmentForId(id));
        }

        public DepartmentResource ChangeLeader(LeaderChangeDto dto) {
            var employee = employeesManager.GetEmployeeForId(dto.EmployeeId);
            var department = Manager.GetDepartmentForId(dto.DepartmentId);
            var leader = Manager.GetDepartmentLeader(department);
            if (leader == null) {
                leader = new DepartmentLeader {Department = department};
            }

            leader.Employee = employee;
            Manager.ChangeLeader(leader);

            return ConvertToDepartmentResource(department);
        }
    }
}
