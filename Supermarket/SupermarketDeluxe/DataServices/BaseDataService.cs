﻿using Supermarket.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supermarket.DataServices {
    public class BaseDataService<TManager> where TManager : BaseManager {
        protected TManager Manager { get; private set; }

        public BaseDataService(TManager manager) {
            this.Manager = manager;
        }

        public static TResource ConvertToResource<TResource, TType>(TType source) where TResource : class, new() {
            var result = new TResource();

            if (source == null) return null;

            var sourceProps = source.GetType().GetProperties();
            var targetProps = result.GetType().GetProperties();

            var availableProps = targetProps.Where(tProp =>
                tProp.CanWrite && sourceProps.Any(sProp => sProp.CanRead && tProp.Name == sProp.Name && tProp.PropertyType == sProp.PropertyType));

            foreach (var prop in availableProps) {
                var sProp = source.GetType().GetProperties().First(x => x.Name == prop.Name);
                prop.SetValue(result, sProp.GetValue(source));
            }

            return result;
        }
    }
}
